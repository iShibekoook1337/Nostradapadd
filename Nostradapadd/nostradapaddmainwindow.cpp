#include "nostradapaddmainwindow.h"
#include "ui_nostradapaddmainwindow.h"

NostradapaddMainWindow::NostradapaddMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::NostradapaddMainWindow)
{
    ui->setupUi(this);
}

NostradapaddMainWindow::~NostradapaddMainWindow()
{
    delete ui;
}
