#ifndef NOSTRADAPADDMAINWINDOW_H
#define NOSTRADAPADDMAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class NostradapaddMainWindow;
}

class NostradapaddMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit NostradapaddMainWindow(QWidget *parent = 0);
    ~NostradapaddMainWindow();

private:
    Ui::NostradapaddMainWindow *ui;
};

#endif // NOSTRADAPADDMAINWINDOW_H
